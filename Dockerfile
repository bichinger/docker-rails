ARG DOCKER_RAILS_BASEIMAGE
FROM ${DOCKER_RAILS_BASEIMAGE}

ONBUILD USER root
# always update the yarn repository key because it may be expired
ONBUILD RUN set -e \
    && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -

ONBUILD ARG NGINX_CONFIG_FOLDER="nginx"
# COPY can handle wildcards but it MUST copy something or it will error. So
# copy the .gitignore as well to guarantee COPY can copy some file. Remove
# the .gitignore from the destination afterwards.
# Note: use array syntax to be able to copy and paste (see block below for
# explanation why we use the array syntax at all)
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/app.conf*", ".gitignore", "/etc/nginx/sites-available/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/app-env-vars.conf*", ".gitignore", "/etc/nginx/main.d/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/server.conf*", ".gitignore", "/etc/nginx/app/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/app-custom.conf*", ".gitignore", "/etc/nginx/app/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/app-env-vars-custom.conf*", ".gitignore", "/etc/nginx/main.d/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/server-custom.conf*", ".gitignore", "/etc/nginx/app/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/passenger-enterprise-license*", ".gitignore", "/etc/"]

ONBUILD ARG NGINX_CONFIG_SUBFOLDER=""
# Note: use array syntax for the totally unlikely but possible case someone
# uses an NGINX_CONFIG_SUBFOLDER with whitespace
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/${NGINX_CONFIG_SUBFOLDER}/app.conf*", ".gitignore", "/etc/nginx/sites-available/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/${NGINX_CONFIG_SUBFOLDER}/app-env-vars.conf*", ".gitignore", "/etc/nginx/main.d/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/${NGINX_CONFIG_SUBFOLDER}/server.conf*", ".gitignore", "/etc/nginx/app/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/${NGINX_CONFIG_SUBFOLDER}/app-custom.conf*", ".gitignore", "/etc/nginx/app/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/${NGINX_CONFIG_SUBFOLDER}/app-env-vars-custom.conf*", ".gitignore", "/etc/nginx/main.d/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/${NGINX_CONFIG_SUBFOLDER}/server-custom.conf*", ".gitignore", "/etc/nginx/app/"]
ONBUILD COPY ["${NGINX_CONFIG_FOLDER}/${NGINX_CONFIG_SUBFOLDER}/passenger-enterprise-license*", ".gitignore", "/etc/"]

ONBUILD RUN set -e \
    && rm -f /etc/nginx/sites-available/.gitignore \
    && rm -f /etc/nginx/main.d/.gitignore \
    && rm -f /etc/nginx/app/.gitignore \
    && rm -f /etc/.gitignore

# when using passenger enterprise, setting this build argument is required
ONBUILD ARG PASSENGER_DOWNLOAD_TOKEN=""
# handle passenger enterprise installation (license file was copied to /etc
# above already)
ONBUILD RUN set -e \
    && if [ -n "${PASSENGER_DOWNLOAD_TOKEN}" -a -f /etc/passenger-enterprise-license ]; then \
            echo "\nInstalling Phusion Passenger™ Enterprise over Community Edition\n" \
            && chmod 644 /etc/passenger-enterprise-license \
            && echo deb https://download:${PASSENGER_DOWNLOAD_TOKEN}@www.phusionpassenger.com/enterprise_apt $(lsb_release -cs) main > /etc/apt/sources.list.d/passenger.list \
            && chmod 644 /etc/apt/sources.list.d/passenger.list \
            && apt-get update -qq \
            && apt-get install -y passenger-enterprise libnginx-mod-http-passenger-enterprise \
        ; fi \
    && rm -f /etc/service/nginx/down \
    && rm /etc/nginx/sites-enabled/default \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# set access_log format for nginx
ONBUILD ARG NGINX_ACCESS_LOG=""
ONBUILD ARG NGINX_ACCESS_LOG_FORMAT="anonymized"
ONBUILD RUN set -e \
    && if [ -n "${NGINX_ACCESS_LOG}" ]; then \
            echo "\nSetting access_log to ${NGINX_ACCESS_LOG}\n" \
            && sed -i -r "s/^([[:space:]]*access_log[[:space:]]).*;[[:space:]]*$/\1 ${NGINX_ACCESS_LOG};/g" /etc/nginx/nginx.conf \
        ; elif [ -n "${NGINX_ACCESS_LOG_FORMAT}" ]; then \
            echo "\nSetting access_log format to ${NGINX_ACCESS_LOG_FORMAT}\n" \
            && sed -i -r "s/^([[:space:]]*access_log[[:space:]]+(\"[^\"]+\"|[^[:space:]]+))([[:space:]]+[^[:space:]]+)?[[:space:]]*;[[:space:]]*$/\1 ${NGINX_ACCESS_LOG_FORMAT};/g" /etc/nginx/nginx.conf \
        ; fi

# set error log level for nginx
ONBUILD ARG NGINX_ERROR_LOG=""
ONBUILD ARG NGINX_ERROR_LOG_LEVEL=""
ONBUILD RUN set -e \
    && if [ -n "${NGINX_ERROR_LOG}" ]; then \
            echo "\nSetting error_log to ${NGINX_ERROR_LOG}\n" \
            && sed -i -r "s/^([[:space:]]*error_log[[:space:]]+)(\"[^\"]+\"|[^[:space:]]+)(([[:space:]]+[^[:space:]]+)?[[:space:]]*);[[:space:]]*$/\1 ${NGINX_ERROR_LOG}\3;/g" /etc/nginx/nginx.conf \
        ; fi \
    && if [ -n "${NGINX_ERROR_LOG_LEVEL}" ]; then \
            echo "\nSetting error_log level to ${NGINX_ERROR_LOG_LEVEL}\n" \
            && sed -i -r "s/^([[:space:]]*error_log[[:space:]]+(\"[^\"]+\"|[^[:space:]]+))([[:space:]]+[^[:space:]]+)?[[:space:]]*;[[:space:]]*$/\1 ${NGINX_ERROR_LOG_LEVEL};/g" /etc/nginx/nginx.conf \
        ; fi

# this is the directory where the application source is copied from. set to "."
# if it is the project root rather than the "src" subdirectory
ONBUILD ARG APP_SOURCE_FOLDER=src
ONBUILD COPY "${APP_SOURCE_FOLDER}" "${APP_HOME}"
ONBUILD RUN chown -R ${APP_USER}:${APP_GROUP} "${APP_HOME}"
ONBUILD WORKDIR "${APP_HOME}"

# explicitly copy .git folder as the configured source_folder may or may not be
# the current dir ('.') and therefore not contain the .git folder. the folder
# is being removed below, after the git revision has been extracted.
ONBUILD COPY .git .git
# extract git revision into file and remove the .git folder
ONBUILD RUN set -e \
    && git config --global --add safe.directory "${APP_HOME}" \
    && echo -n $(git show --format="%H" --no-patch) | tee "${APP_GIT_REVISION_FILE}" /etc/container_environment/APP_GIT_REVISION \
    && git config --global --unset safe.directory \
    && rm -rf .git \
    && mkdir -p tmp \
    && touch tmp/restart.txt \
    && chown -R ${APP_USER}:${APP_GROUP} tmp

ONBUILD USER ${APP_USER}
